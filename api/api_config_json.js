var live_api = true; //This will connect to the development live api endpoint which connects to the live dev DB
if (live_api) {
    var api_ends = {
        api_end_point:
            'https://4l0nq44u0k.execute-api.us-east-2.amazonaws.com/staging/api/',
    };
} else {
    var api_ends = {
        api_end_point: 'http://localhost:3000/api/',
    };
}
console.log('Using live Api: ', live_api);
export default api_ends;
