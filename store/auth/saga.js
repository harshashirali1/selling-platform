import { all, put, takeEvery } from 'redux-saga/effects';
import { notification } from 'antd';

import {
    actionTypes,
    registerSuccess,
    loginSuccess,
    logOutSuccess,
} from './action';

const modalSuccess = (type) => {
    notification[type]({
        message: 'Welcome back',
        description: 'You are logged in successfully!',
    });
};

const modalRegister = (type) => {
    notification[type]({
        message: 'Welcome!!!',
        description: 'Your registration was successful!',
    });
};

const modalWarning = (type) => {
    notification[type]({
        message: 'Good bye!',
        description: 'Your account has been Logged out!',
    });
};

function* registerSaga(data) {
    try {
        console.log('RegSaga 1', data);
        yield put(registerSuccess(data.payload));

        console.log('RegSaga 2');
        modalRegister('success');
    } catch (err) {
        console.log(err);
    }
}

function* loginSaga(data) {
    try {
        console.log('Sagas 1', data);
        yield put(loginSuccess(data.payload));
        console.log('Saga 2');
        modalSuccess('success');
    } catch (err) {
        console.log(err);
    }
}

function* logOutSaga() {
    try {
        yield put(logOutSuccess());
        modalWarning('warning');
    } catch (err) {
        console.log(err);
    }
}

export default function* rootSaga() {
    yield all([takeEvery(actionTypes.REGISTER, registerSaga)]);
    yield all([takeEvery(actionTypes.LOGIN_REQUEST, loginSaga)]);
    yield all([takeEvery(actionTypes.LOGOUT, logOutSaga)]);
}
