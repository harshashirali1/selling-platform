import { actionTypes } from './action';

export const initState = {
    isLoggedIn: false,
};

function reducer(state = initState, action) {
    switch (action.type) {
        case actionTypes.REGISTER_SUCCESS:
            console.log({ action });
            return {
                ...state,
                ...action.payload,
            };
        case actionTypes.LOGIN_SUCCESS:
            console.log({ action });
            return {
                ...state,
                ...action.payload,
            };
        case actionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                // ...{ isLoggedIn: false },
                ...{
                    isLoggedIn: false,
                    email: '',
                    token: '',
                    accessLevel: '',
                },
            };
        default:
            return state;
    }
}

export default reducer;
