export const actionTypes = {
    REGISTER: 'REGISTER',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    LOGIN_REQUEST: 'LOGIN_REQUEST',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGOUT: 'LOGOUT',
    LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
    CHECK_AUTHORIZATION: 'CHECK_AUTHORIZATION',
};

export function register(payload) {
    console.log('reg payload', payload);
    return { type: actionTypes.REGISTER, payload };
}

export function registerSuccess(payload) {
    console.log('Here now in reg');
    return { type: actionTypes.REGISTER_SUCCESS, payload };
}

export function login(payload) {
    console.log('payload000', payload);
    return { type: actionTypes.LOGIN_REQUEST, payload };
}

export function loginSuccess(payload) {
    console.log('Here now');
    return { type: actionTypes.LOGIN_SUCCESS, payload };
}

export function logOut() {
    return { type: actionTypes.LOGOUT };
}

export function logOutSuccess() {
    return { type: actionTypes.LOGOUT_SUCCESS };
}

// export function checkAuthorization(payload) {
//     return { type: actionTypes.CHECK_AUTHORIZATION, payload };
// }
