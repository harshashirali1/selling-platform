//HAVING ISSUES WITH RUNNING THE CODE? OR OTHER ISSUE? USE THE LINK BELOW
* Project Troubleshooting doc: https://docs.google.com/document/d/1FGG8R9DmqqV-v5yZl00j5KUCtjI2bnB5W8nJLZF5ILA/edit?usp=sharing 

WORKFLOW
1. Task assigned
2. Task completed
3. run e2e tests (npm run e2e)
4. Submit task for review. (push to branch YOURNAME-TASKNAME) (INFORM Aaron) 
*  Perform if 3. passed 
*  If failed, return to 1. (fixed failed items)
5. Ufere merge, manual test & run e2e tests (npm run e2e)
*  Perform if 4. passed 
*  if failed, return to 1. (fixed failed items)
6. Aaron manual test & run e2e tests (npm run e2e)
*  Perform if 5. passed 
*  if failed, return to 1. (fixed failed items)

