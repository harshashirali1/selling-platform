// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('getUserAccessLevel', () => {
    cy.intercept('POST', '/user_access_level_get', (req) => {
        if (req.body === 'id_user=0') {
            req.reply(async (res) => {
                await res.send({
                    success: true,
                    data: {
                        id_access_levels: [0, 1, 2, 3, 4, 5],
                    },
                });
            });
        } else if (req.body === 'id_user=1') {
            req.reply(async (res) => {
                await res.send({
                    success: true,
                    data: {
                        id_access_levels: [1, 3, 4, 5],
                    },
                });
            });
        } else if (req.body === 'id_user=1') {
            req.reply(async (res) => {
                await res.send({
                    success: true,
                    data: {
                        id_access_levels: [2, 3, 4, 5],
                    },
                });
            });
        }
    }).as('loadPage');
});

Cypress.Commands.add('headerNavigationLink', (text) => {
    cy.get('header[id="headerSticky"]').within(() => {
        cy.contains(text).click({ force: true });
    });
});

Cypress.Commands.add('footerNavigationLink', (text) => {
    cy.get('footer[class="ps-footer"]').within(() => {
        cy.contains(text).click({ force: true });
    });
});

Cypress.Commands.add('breadcrumbLink', (text) => {
    cy.get('.ps-breadcrumb').within(() => {
        cy.contains(text).click();
    });
});

Cypress.Commands.add('brandLink', (text) => {
    cy.get('.ps-product__meta').within(() => {
        cy.get('a').click({ force: true });
    });
});

Cypress.Commands.add('soldByLink', (text) => {
    cy.get('.ps-product__desc').within(() => {
        cy.get('a').click({ force: true });
    });
});

Cypress.Commands.add('productShoppingLink', (text) => {
    cy.get('.ps-product__shopping').within(() => {
        cy.contains(text).click();
    });
});

Cypress.Commands.add('linksIn', (elem, index) => {
    cy.get(elem).then(($li) => {
        cy.wrap($li[index]).click();
    });
});

Cypress.Commands.add('logIn', (user_login, user_password) => {
    cy.get('#user_login').type(user_login);
    cy.get('#user_password').type(user_password);
    cy.get("button[type='submit']").click({ force: true });
});
