describe('USER_VENDOR_TM_LEVEL', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('USER_VENDOR_TM_LEVEL');
    });

    it('Login required', () => {
        cy.get('#sign-in').should('be.visible').should('be.visible');
    });

    it('Logging in', () => {
        cy.logIn('Bret', 0);
    });

    it('User-Vendor-TM access', () => {
        cy.getUserAccessLevel();
    });
});
