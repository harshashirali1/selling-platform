describe('USER_BUYER_ACCESS_LEVEL', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('USER_BUYER_ACCESS_LEVEL');
    });

    it('Login required', () => {
        cy.get('#sign-in').should('be.visible').should('be.visible');
    });

    it('Logging in', () => {
        cy.logIn('Bret', 0);
    });

    it('User-Buyer access', () => {
        cy.getUserAccessLevel();
    });
});
