describe('PUBLIC_ACCESS_LEVEL', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('PUBLIC_ACCESS_LEVEL');
    });

    it('Public access', () => {
        cy.getUserAccessLevel();
    });
});
