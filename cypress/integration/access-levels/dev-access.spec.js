describe('DEV_ACCESS_LEVEL', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('DEV_ACCESS_LEVEL');
    });

    let outterLayer;
    let innerLayer;
    it('Get login status', () => {
        cy.window().then(() => {
            let res = localStorage.getItem('persist:martfury');
            res ? (outterLayer = JSON.parse(res)) : null;
            outterLayer ? (innerLayer = JSON.parse(outterLayer.auth)) : null;
        });
    });

    if (innerLayer?.isLoggedIn) {
        it('logged In', () => {});
    } else {
        it('Login required', () => {
            cy.get('#sign-in').should('be.visible');
        });

        it('logging in', () => {
            cy.logIn('Bret', 0);
        });
    }

    it('Dev access', () => {
        cy.getUserAccessLevel();
    });
});
