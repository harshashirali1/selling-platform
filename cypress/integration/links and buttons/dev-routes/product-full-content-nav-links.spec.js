describe('Route Dev Routes: Product Full Content navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('Product Full Content');
        cy.location('pathname').should('eq', '/product/full-content/7');
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.location('pathname').should('eq', '/');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/full-content/7');
    });

    it('Shop', () => {
        cy.breadcrumbLink('Shop');
        cy.location('pathname').should('eq', '/shop');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/full-content/7');
    });

    it('Brand:', () => {
        cy.brandLink();
        cy.go('back');
        cy.location('pathname').should('eq', '/product/full-content/7');
    });

    it('Sold By:', () => {
        cy.soldByLink();
        cy.go('back');
        cy.location('pathname').should('eq', '/product/full-content/7');
    });

    it('Increment & decrement quantity', () => {
        cy.get('.ps-product__price-right').within(() => {
            cy.get('button').click({ multiple: true, force: true });
        });
    });

    it('Add to cart', () => {
        cy.get('.ps-product__price-right').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[0]).click();
            });
        });
    });

    it('Buy Now', () => {
        cy.get('.ps-product__price-right').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[1]).click();
            });
        });
    });

    it('Submit Review', () => {
        cy.contains('Submit Review').click();
    });
});
