describe('Route Dev Routes: Simple Product Type navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('Simple Product Type');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });
});
