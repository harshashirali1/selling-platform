describe('Route Dev Routes: Shop Catalog Carousel navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('Shop Catalog Carousel');
        cy.location('pathname').should('eq', '/shop/shop-carousel');
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.location('pathname').should('eq', '/');
        cy.go('back');
        cy.location('pathname').should('eq', '/shop/shop-carousel');
    });
});
