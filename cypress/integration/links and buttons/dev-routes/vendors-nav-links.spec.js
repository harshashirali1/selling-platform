describe('Vendors navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('Vendors');
        cy.location('pathname').should('eq', '/vendor/vendor-store');
    });

    it('Contact Seller', () => {
        cy.contains('Contact Seller').click();
    });
});
