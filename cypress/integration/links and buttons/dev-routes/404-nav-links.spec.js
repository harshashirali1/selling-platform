describe('404 navigation links', () => {
    before(() => {
        cy.visit('');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('404');
        cy.location('pathname').should('eq', '/404');
    });

    it('Homepage', () => {
        cy.get('.ps-page--404').within(() => {
            cy.get('a').click();
        });
    });
});
