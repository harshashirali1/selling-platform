describe('Route Dev Routes: Products Countdown navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('Products Countdown');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });

    it('Shop', () => {
        cy.breadcrumbLink('Shop');
        cy.location('pathname').should('eq', '/shop');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });

    it('Brand:', () => {
        cy.get('.ps-product__meta').within(() => {
            cy.get('a').click();
        });
        cy.go('back');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });

    it('Sold By:', () => {
        cy.get('.ps-product__desc').within(() => {
            cy.get('a').click({ force: true });
        });
        cy.go('back');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });

    it('Add to cart', () => {
        cy.get('.ps-product__shopping').within(() => {
            cy.contains('Add to cart').click({ force: true });
        });
    });

    it('Buy Now', () => {
        cy.get('.ps-product__shopping').within(() => {
            cy.contains('Buy Now').click();
        });
        cy.location('pathname').should('eq', '/account/checkout');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/countdown/10');
    });

    it('Description', () => {
        cy.get('.ant-tabs-nav-list').within(() => {
            cy.contains('Description').click();
        });
    });

    it('Specification', () => {
        cy.get('.ant-tabs-nav-list').within(() => {
            cy.contains('Specification').click();
        });
    });

    it('Vendor', () => {
        cy.get('.ant-tabs-nav-list').within(() => {
            cy.contains('Vendor').click();
        });
    });

    it('Review', () => {
        cy.get('.ant-tabs-nav-list').within(() => {
            cy.contains('Review').click();
        });
    });

    it('Questions and Answers', () => {
        cy.get('.ant-tabs-nav-list').within(() => {
            cy.contains('Questions and Answers').click();
        });
    });
});
