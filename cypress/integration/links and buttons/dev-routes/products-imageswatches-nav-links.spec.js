describe('Route Dev Routes: Products Imageswatches navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('Products Imageswatches');
        cy.location('pathname').should('eq', '/product/image-swatches/11');
    });

    it('Login required', () => {
        cy.get('#sign-in').should('be.visible');
    });

    it('Logging in', () => {
        cy.logIn('Henry', 0);
    });

    it('Gai', () => {
        cy.getUserAccessLevel();
        cy.wait('@loadPage');
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/image-swatches/11');
    });

    it('Shop', () => {
        cy.breadcrumbLink('Shop');
        cy.go('back');
        cy.location('pathname').should('eq', '/product/image-swatches/11');
    });

    it('Brand:', () => {
        cy.brandLink();
        cy.go('back');
        cy.location('pathname').should('eq', '/product/image-swatches/11');
    });

    it('Sold By:', () => {
        cy.soldByLink();
        cy.go('back');
        cy.location('pathname').should('eq', '/product/image-swatches/11');
    });

    it('Add to cart', () => {
        cy.productShoppingLink('Add to cart');
    });

    it('Buy Now', () => {
        cy.productShoppingLink('Buy Now');
    });

    it('Description', () => {
        cy.linksIn('.ant-tabs-tab', 0);
    });

    it('Specification', () => {
        cy.linksIn('.ant-tabs-tab', 1);
    });

    it('Vendor', () => {
        cy.linksIn('.ant-tabs-tab', 2);
    });

    it('Review', () => {
        cy.linksIn('.ant-tabs-tab', 3);
    });

    it('Questions and Answers', () => {
        cy.linksIn('.ant-tabs-tab', 4);
    });
});
