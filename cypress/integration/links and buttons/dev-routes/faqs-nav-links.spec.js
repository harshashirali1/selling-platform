describe('Route Dev Routes: FAQs navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.headerNavigationLink('FAQs');
        cy.location('pathname').should('eq', '/page/faqs');
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.location('pathname').should('eq', '/');
        cy.go('back');
        cy.location('pathname').should('eq', '/page/faqs');
    });
});
