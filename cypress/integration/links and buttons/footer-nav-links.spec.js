describe('Footer navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('FAQs', () => {
        cy.footerNavigationLink('FAQ');
        cy.url().should('include', '/faqs');
    });

    it('About Us', () => {
        cy.footerNavigationLink('About Us');
        cy.url().should('include', '/page/about-us');
    });

    it('Contact', () => {
        cy.get('footer[class="ps-footer"]').within(() => {
            cy.get('li').contains('Contact').click();
        });
        cy.url().should('include', '/page/contact-us');
    });

    it('Checkout', () => {
        cy.footerNavigationLink('Checkout');
        cy.url().should('include', '/account/checkout');
    });
});
