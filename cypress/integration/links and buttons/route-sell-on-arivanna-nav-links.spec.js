describe('Route Sell on Arivanna navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.contains('Sell on Arivanna').click({ force: true });
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.go('back');
    });

    it('Start selling', () => {
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[1]).click();
            });
        });
        cy.go('back');
    });

    it('Low fees: learn more', () => {
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[2]).click();
            });
        });
        cy.go('back');
    });

    it('Powerful tools: learn more', () => {
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[3]).click();
            });
        });
        cy.go('back');
    });

    it('Support 24/7: learn more', () => {
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[4]).click();
            });
        });
        cy.go('back');
    });

    it('--5', () => {
        //prev
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[5]).click();
            });
        });
        cy.go('back');
    });

    it('--6', () => {
        //next
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[6]).click();
            });
        });
    });

    it('Contact us', () => {
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[7]).click();
            });
        });
        cy.go('back');
    });

    it('Start selling', () => {
        cy.get('main').within(() => {
            cy.get('a').then(($li) => {
                cy.wrap($li[8]).click();
            });
        });
    });
});
