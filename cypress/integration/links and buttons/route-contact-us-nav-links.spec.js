describe('Route Contact Us navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.contains('Contact Us').click({ force: true });
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
        cy.go('back');
    });

    it('Send message', () => {
        cy.contains('Send message').click();
    });
});
