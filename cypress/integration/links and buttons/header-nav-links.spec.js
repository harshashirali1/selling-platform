describe('Header navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Wishlist', () => {
        cy.get('header[id="headerSticky"]').within(() => {
            cy.get('a[href*="/account/wishlist"]').click();
        });
        cy.url().should('include', '/account/wishlist');
    });

    it('Cart', () => {
        cy.get('header[id="headerSticky"]').within(() => {
            cy.get('i[class="icon-bag2"]').click();
        });
    });

    it('Login', () => {
        cy.headerNavigationLink('Login');
        cy.url().should('include', '/account/login');
    });

    it('Register', () => {
        cy.headerNavigationLink('Register');
        cy.url().should('include', '/account/register');
    });

    it('Products', () => {
        cy.headerNavigationLink('Products');
        cy.url().should('include', '/product/countdown/10');
    });

    it('About Us', () => {
        cy.headerNavigationLink('About Us');
        cy.url().should('include', '/page/about-us');
    });

    it('Contact Us', () => {
        cy.headerNavigationLink('Contact Us');
        cy.url().should('include', '/page/contact-us');
    });

    it('Stores', () => {
        cy.headerNavigationLink('Stores');
        cy.url().should('include', '/stores');
    });

    it('Sell on Arivanna', () => {
        cy.headerNavigationLink('Sell on Arivanna');
        cy.url().should('include', '/vendor/become-a-vendor');
    });

    it('Track your order', () => {
        cy.headerNavigationLink('Track your order');
        cy.url().should('include', '/account/order-tracking');
    });

    it('Dev Routes: 404', () => {
        cy.headerNavigationLink('404');
    });

    it('Dev Routes: Vendors', () => {
        cy.headerNavigationLink('Vendors');
    });

    it('Dev Routes: Products Countdown', () => {
        cy.headerNavigationLink('Products Countdown');
    });

    it('Dev Routes: Products Imageswatches', () => {
        cy.headerNavigationLink('Products Imageswatches');
    });

    it('Dev Routes: Product Full Content', () => {
        cy.headerNavigationLink('Product Full Content');
    });

    it('Dev Routes: Simple Product Type', () => {
        cy.headerNavigationLink('Simple Product Type');
    });

    it('Dev Routes: Shop', () => {
        cy.headerNavigationLink('Shop');
    });

    it('Dev Routes: Shop Catalog Carousel', () => {
        cy.headerNavigationLink('Shop Catalog Carousel');
    });

    it('Dev Routes: FAQs', () => {
        cy.headerNavigationLink('FAQs');
        cy.url().should('include', '/faqs');
    });
});
