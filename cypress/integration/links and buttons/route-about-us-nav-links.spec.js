describe('Route About Us navigation links', () => {
    before(() => {
        cy.visit('/');
    });

    it('Go to route', () => {
        cy.contains('About Us').click({ force: true });
    });

    it('Home', () => {
        cy.breadcrumbLink('Home');
    });
});
