describe('Should register user', () => {
    const firstName = 'Kobe';
    const lastName = 'Bryant';
    const middleName = 'Linus';
    const email = 'kobe@mail.com';
    const dob = '1985-05-11';
    const gender = 'male';
    const addressOne = 'texas';
    const addressTwo = 'california';
    const phone = 676799;
    const password = 'bryant';

    it('successfully', () => {
        cy.visit('/account/register', { timeout: 20000 });
        cy.get('input[name="user_first_name"]').type(firstName);
        cy.get('input[name="user_middle_name"]').type(middleName);
        cy.get('input[name="user_last_name"]').type(lastName);
        cy.get('input[name="user_dob"]').type(dob);
        cy.get('input[name="user_gender"]').type(gender);
        cy.get('input[name="user_address_shipping"]').type(addressOne);
        cy.get('input[name="user_address_billing"]').type(addressTwo);
        cy.get('input[name="user_phone_number"]').type(phone);
        cy.get('input[name="user_email"]').type(email);
        cy.get('input[name="user_password"]').type(password);
        cy.get('button[type="submit"]').click({ timeout: 10000 });

        cy.intercept(
            'POST',
            'https://4l0nq44u0k.execute-api.us-east-2.amazonaws.com/staging/api/user_create'
        ).as('registering');

        cy.wait('@registering');

        cy.location('pathname').should('eq', '/');
    });
});
