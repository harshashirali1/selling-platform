import React, { Component } from 'react';
import { notification } from 'antd';
class LanguageSwitcher extends Component {
    constructor(props) {
        super(props);
    }

    handleFeatureWillUpdate(e) {
        e.preventDefault();
        notification.open({
            message: 'Opp! Something went wrong.',
            description: 'This feature has been updated later!',
            duration: 500,
        });
    }

    render() {
        return (
            <div className="language">
                <a href="#">
                    <img src="/static/img/flag/en.png" alt="martfury" />
                    English
                </a>
            </div>
        );
    }
}

export default LanguageSwitcher;
