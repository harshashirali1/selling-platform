import React, { Component } from 'react';
import Link from 'next/link';
import { notification } from 'antd';
import Menu from '~/components/elements/menu/Menu';
// import Menu from '../../elements/menu/Menu';

import menuData from '../../../public/static/data/menu';
import CurrencyDropdown from '../headers/modules/CurrencyDropdown';
import LanguageSwitcher from '../headers/modules/LanguageSwitcher'; 

const menuTechnology = [
    {
        text: 'Home',
        url: '/', 
    },
    {
        text: 'Products',
        url: '/product/countdown/10', 
    },
    {
        text: 'About Us',
        url: '/page/about-us',
    },
    {
        text: 'Contact Us',
        url: '/page/contact-us',
    },
    {
        text: 'Stores',
        url: '/stores',
    },
    {
        text: 'FAQs',
        url: '/page/faqs',
    },

];
const menuTechnologyTablet = [
    {
        text: 'Home',
        url: '/', 
    },
    {
        text: 'Products',
        url: '/product/countdown/10',
    },
    {
        text: 'About Us',
        url: '/page/about-us',
    },
    {
        text: 'Contact Us',
        url: '/page/contact-us',
    },
    {
        text: 'Stores',
        url: '/stores',
    },
];
const menuTechnologyMiniTablet = [
    {
        text: 'Home',
        url: '/', 
    },
    {
        text: 'Products',
        url: '/product/countdown/10',
    },
    {
        text: 'About Us',
        url: '/page/about-us',
    },
    {
        text: 'Stores',
        url: '/stores',
    },
];
const menuTechnologyMicroTablet = [
    {
        text: 'Home',
        url: '/', 
    },
    {
        text: 'Products',
        url: '/product/countdown/10',
    },
    {
        text: 'About Us',
        url: '/page/about-us',
    },
];
const menuTechnologySmallTablet = [
    {
        text: 'Home',
        url: '/', 
    },
    {
        text: 'Products',
        url: '/product/countdown/10',
    },
];
const menuTechnologyExtraSmallTablet = [
    {
        text: 'Home',
        url: '/', 
    },
];
const devRoutes = [
    {
        text: '404',
        url: '/404', 
    },
    {
        text: 'Vendors',
        url: '/vendor/vendor-store',
    },
    {
        text: 'Products Countdown',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Imageswatches',
        url: '/product/image-swatches/11',
    },
    {
        text: 'Product Full Content',
        url: '/product/full-content/7',
    },
    {
        text: 'Simple Product Type',
        url: '/product/countdown/10',
    },
    {
        text: 'Shop',
        url: '/shop',
    },
    {
        text: 'Shop Catalog Carousel',
        url: '/shop/shop-carousel',
    },
    {
        text: 'ALWAYS_ACCESS_LEVEL',
        url: '/authorization_tests/ALWAYS_ACCESS_LEVEL',
    },
    {
        text: 'DEV_ACCESS_LEVEL',
        url: '/authorization_tests/DEV_ACCESS_LEVEL',
    },
    {
        text: 'PUBLIC_ACCESS_LEVEL',
        url: '/authorization_tests/PUBLIC_ACCESS_LEVEL',
    },
    {
        text: 'USER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_ACCESS_LEVEL',
    },
    {
        text: 'USER_BUYER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_BUYER_ACCESS_LEVEL',
    },
    {
        text: 'USER_VENDOR_TM_LEVEL',
        url: '/authorization_tests/USER_VENDOR_TM_LEVEL',
    }


];
const devRoutesTablet = [
    {
        text: '404',
        url: '/404', 
    },
    {
        text: 'Vendors',
        url: '/vendor/vendor-store',
    },
    {
        text: 'Products Countdown',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Imageswatches',
        url: '/product/image-swatches/11',
    },
    {
        text: 'Product Full Content',
        url: '/product/full-content/7',
    },
    {
        text: 'Simple Product Type',
        url: '/product/countdown/10',
    },
    {
        text: 'Shop',
        url: '/shop',
    },
    {
        text: 'Shop Catalog Carousel',
        url: '/shop/shop-carousel',
    },
    {
        text: 'FAQs',
        url: '/page/faqs',
    },
    {
        text: 'ALWAYS_ACCESS_LEVEL',
        url: '/authorization_tests/ALWAYS_ACCESS_LEVEL',
    },
    {
        text: 'DEV_ACCESS_LEVEL',
        url: '/authorization_tests/DEV_ACCESS_LEVEL',
    },
    {
        text: 'PUBLIC_ACCESS_LEVEL',
        url: '/authorization_tests/PUBLIC_ACCESS_LEVEL',
    },
    {
        text: 'USER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_ACCESS_LEVEL',
    },
    {
        text: 'USER_BUYER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_BUYER_ACCESS_LEVEL',
    },
    {
        text: 'USER_VENDOR_TM_LEVEL',
        url: '/authorization_tests/USER_VENDOR_TM_LEVEL',
    }

];
const devRoutesMiniTablet = [
    {
        text: '404',
        url: '/404', 
    },
    {
        text: 'Vendors',
        url: '/vendor/vendor-store',
    },
    {
        text: 'Products Countdown',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Imageswatches',
        url: '/product/image-swatches/11',
    },
    {
        text: 'Product Full Content',
        url: '/product/full-content/7',
    },
    {
        text: 'Simple Product Type',
        url: '/product/countdown/10',
    },
    {
        text: 'Shop',
        url: '/shop',
    },
    {
        text: 'Shop Catalog Carousel',
        url: '/shop/shop-carousel',
    },
    {
        text: 'FAQs',
        url: '/page/faqs',
    },
    {
        text: 'Contact Us',
        url: '/page/contact-us',
    },
    {
        text: 'ALWAYS_ACCESS_LEVEL',
        url: '/authorization_tests/ALWAYS_ACCESS_LEVEL',
    },
    {
        text: 'DEV_ACCESS_LEVEL',
        url: '/authorization_tests/DEV_ACCESS_LEVEL',
    },
    {
        text: 'PUBLIC_ACCESS_LEVEL',
        url: '/authorization_tests/PUBLIC_ACCESS_LEVEL',
    },
    {
        text: 'USER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_ACCESS_LEVEL',
    },
    {
        text: 'USER_BUYER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_BUYER_ACCESS_LEVEL',
    },
    {
        text: 'USER_VENDOR_TM_LEVEL',
        url: '/authorization_tests/USER_VENDOR_TM_LEVEL',
    }
];
const devRoutesMicroTablet = [
    {
        text: '404',
        url: '/404', 
    },
    {
        text: 'Vendors',
        url: '/vendor/vendor-store',
    },
    {
        text: 'Products Countdown',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Imageswatches',
        url: '/product/image-swatches/11',
    },
    {
        text: 'Product Full Content',
        url: '/product/full-content/7',
    },
    {
        text: 'Simple Product Type',
        url: '/product/countdown/10',
    },
    {
        text: 'Shop',
        url: '/shop',
    },
    {
        text: 'Shop Catalog Carousel',
        url: '/shop/shop-carousel',
    },
    {
        text: 'FAQs',
        url: '/page/faqs',
    },
    {
        text: 'Contact Us',
        url: '/page/contact-us',
    },
    {
        text: 'Stores',
        url: '/stores',
    },
    {
        text: 'ALWAYS_ACCESS_LEVEL',
        url: '/authorization_tests/ALWAYS_ACCESS_LEVEL',
    },
    {
        text: 'DEV_ACCESS_LEVEL',
        url: '/authorization_tests/DEV_ACCESS_LEVEL',
    },
    {
        text: 'PUBLIC_ACCESS_LEVEL',
        url: '/authorization_tests/PUBLIC_ACCESS_LEVEL',
    },
    {
        text: 'USER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_ACCESS_LEVEL',
    },
    {
        text: 'USER_BUYER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_BUYER_ACCESS_LEVEL',
    },
    {
        text: 'USER_VENDOR_TM_LEVEL',
        url: '/authorization_tests/USER_VENDOR_TM_LEVEL',
    }
];
const devRoutesSmall = [
    {
        text: '404',
        url: '/404', 
    },
    {
        text: 'Vendors',
        url: '/vendor/vendor-store',
    },
    {
        text: 'Products Countdown',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Imageswatches',
        url: '/product/image-swatches/11',
    },
    {
        text: 'Product Full Content',
        url: '/product/full-content/7',
    },
    {
        text: 'Simple Product Type',
        url: '/product/countdown/10',
    },
    {
        text: 'Shop',
        url: '/shop',
    },
    {
        text: 'Shop Catalog Carousel',
        url: '/shop/shop-carousel',
    },
    {
        text: 'FAQs',
        url: '/page/faqs',
    },
    {
        text: 'Contact Us',
        url: '/page/contact-us',
    },
    {
        text: 'Stores',
        url: '/stores',
    },
    {
        text: 'About Us',
        url: '/page/about-us',
    },
    {
        text: 'ALWAYS_ACCESS_LEVEL',
        url: '/authorization_tests/ALWAYS_ACCESS_LEVEL',
    },
    {
        text: 'DEV_ACCESS_LEVEL',
        url: '/authorization_tests/DEV_ACCESS_LEVEL',
    },
    {
        text: 'PUBLIC_ACCESS_LEVEL',
        url: '/authorization_tests/PUBLIC_ACCESS_LEVEL',
    },
    {
        text: 'USER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_ACCESS_LEVEL',
    },
    {
        text: 'USER_BUYER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_BUYER_ACCESS_LEVEL',
    },
    {
        text: 'USER_VENDOR_TM_LEVEL',
        url: '/authorization_tests/USER_VENDOR_TM_LEVEL',
    }
];
const devRoutesExtraSmall = [
    {
        text: '404',
        url: '/404', 
    },
    {
        text: 'Vendors',
        url: '/vendor/vendor-store',
    },
    {
        text: 'Products',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Countdown',
        url: '/product/countdown/10',
    },
    {
        text: 'Products Imageswatches',
        url: '/product/image-swatches/11',
    },
    {
        text: 'Product Full Content',
        url: '/product/full-content/7',
    },
    {
        text: 'Simple Product Type',
        url: '/product/countdown/10',
    },
    {
        text: 'Shop',
        url: '/shop',
    },
    {
        text: 'Shop Catalog Carousel',
        url: '/shop/shop-carousel',
    },
    {
        text: 'FAQs',
        url: '/page/faqs',
    },
    {
        text: 'Contact Us',
        url: '/page/contact-us',
    },
    {
        text: 'Stores',
        url: '/stores',
    },
    {
        text: 'About Us',
        url: '/page/about-us',
    },
    {
        text: 'ALWAYS_ACCESS_LEVEL',
        url: '/authorization_tests/ALWAYS_ACCESS_LEVEL',
    },
    {
        text: 'DEV_ACCESS_LEVEL',
        url: '/authorization_tests/DEV_ACCESS_LEVEL',
    },
    {
        text: 'PUBLIC_ACCESS_LEVEL',
        url: '/authorization_tests/PUBLIC_ACCESS_LEVEL',
    },
    {
        text: 'USER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_ACCESS_LEVEL',
    },
    {
        text: 'USER_BUYER_ACCESS_LEVEL',
        url: '/authorization_tests/USER_BUYER_ACCESS_LEVEL',
    },
    {
        text: 'USER_VENDOR_TM_LEVEL',
        url: '/authorization_tests/USER_VENDOR_TM_LEVEL',
    }
];

class NavigationDefault extends Component {
    constructor(props) {
        super(props);
    }

    handleFeatureWillUpdate(e) {
        e.preventDefault();
        notification.open({
            message: 'Opp! Something went wrong.',
            description: 'This feature has been updated later!', 
            duration: 500,
        });
    }


    render() {
        return (
            <>
            <nav className="navigation">
                <div className="ps-container">
                    <div className="navigation__right">
                        <Menu
                            source={menuTechnology}
                            className="menu menu--technology"
                        />
                        <div className="dev-routes-categories dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Dev Routes <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={devRoutes}
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        <div className="dev-routes-categories dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Shop By Department <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={menuData.product_categories} 
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        {/* <Menu
                            source={menuData.menuPrimary.menu_1}
                            className="menu"
                        /> */}
                        <ul className="navigation__extra">
                            <li className="nav_extra_arivanna">
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Arivanna</a>
                                </Link>
                            </li>
                            <li className="nav_extra_arivanna">
                                <Link href="/account/order-tracking">
                                    <a>Track your order</a>
                                </Link>
                            </li>
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li>
                                <LanguageSwitcher />
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navigation-tablet">
                <div className="ps-container">
                    <div className="navigation__right">
                        <Menu
                            source={menuTechnologyTablet}
                            className="menu menu--technology"
                        />
                        <div className="dev-routes-categories dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Dev Routes <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={devRoutesTablet}
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        <div className="dev-routes-categories dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Shop By Department <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={menuData.product_categories} 
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        {/* <Menu
                            source={menuData.menuPrimary.menu_1}
                            className="menu"
                        /> */}
                        <ul className="navigation__extra">
                            <li className="nav_extra_arivanna">
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Arivanna</a>
                                </Link>
                            </li>
                            <li className="nav_extra_arivanna">
                                <Link href="/account/order-tracking">
                                    <a>Track your order</a>
                                </Link>
                            </li>
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li>
                                <LanguageSwitcher />
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navigation-mini-tablet">
                <div className="ps-container">
                    <div className="navigation__right">
                        <Menu
                            source={menuTechnologyMiniTablet}
                            className="menu menu--technology menu-mini-tablet"
                        />
                        <div className="dev-routes-categories dev-routes mini-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Dev Routes <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={devRoutesMiniTablet}
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        <div className="dev-routes-categories dev-routes mini-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Shop By Department <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={menuData.product_categories} 
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        {/* <Menu
                            source={menuData.menuPrimary.menu_1}
                            className="menu"
                        /> */}
                        <ul className="navigation__extra">
                            <li className="nav_extra_arivanna">
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Arivanna</a>
                                </Link>
                            </li>
                            <li className="nav_extra_arivanna">
                                <Link href="/account/order-tracking">
                                    <a>Track your order</a>
                                </Link>
                            </li>
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li>
                                <LanguageSwitcher />
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navigation-micro-tablet">
                <div className="ps-container">
                    <div className="navigation__right">
                        <Menu
                            source={menuTechnologyMicroTablet}
                            className="menu menu--technology menu-micro-tablet"
                        />
                        <div className="dev-routes-categories dev-routes micro-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Dev Routes <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={devRoutesMicroTablet}
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        <div className="dev-routes-categories dev-routes micro-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Shop By Department <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={menuData.product_categories} 
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        {/* <Menu
                            source={menuData.menuPrimary.menu_1}
                            className="menu"
                        /> */}
                        <ul className="navigation__extra">
                            <li className="nav_extra_arivanna">
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Arivanna</a>
                                </Link>
                            </li>
                            <li className="nav_extra_arivanna">
                                <Link href="/account/order-tracking">
                                    <a>Track your order</a>
                                </Link>
                            </li>
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li>
                                <LanguageSwitcher />
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navigation-micro-tablet-small">
                <div className="ps-container">
                    <div className="navigation__right">
                        <Menu
                            source={menuTechnologySmallTablet}
                            className="menu menu--technology menu-micro-tablet"
                        />
                        <div className="dev-routes-categories dev-routes micro-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Dev Routes <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={devRoutesSmall}
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        <div className="dev-routes-categories dev-routes micro-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Shop By Department <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={menuData.product_categories} 
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        {/* <Menu
                            source={menuData.menuPrimary.menu_1}
                            className="menu"
                        /> */}
                        <ul className="navigation__extra">
                            <li className="nav_extra_arivanna">
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Arivanna</a>
                                </Link>
                            </li>
                            <li className="nav_extra_arivanna">
                                <Link href="/account/order-tracking">
                                    <a>Track your order</a>
                                </Link>
                            </li>
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li>
                                <LanguageSwitcher />
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navigation-micro-tablet-extra-small">
                <div className="ps-container">
                    <div className="navigation__right">
                        <Menu
                            source={menuTechnologyExtraSmallTablet}
                            className="menu menu--technology menu-micro-tablet"
                        />
                        <div className="dev-routes-categories dev-routes micro-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Dev Routes <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={devRoutesExtraSmall}
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        <div className="dev-routes-categories dev-routes micro-dev-routes">
                            <div className="menu__toggle dev-routes-toggle">
                            Shop By Department <i className="icon-menu dev-routes-menu-icon"></i>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    source={menuData.product_categories} 
                                    className="menu--dropdown" 
                                />
                            </div>
                        </div>
                        {/* <Menu
                            source={menuData.menuPrimary.menu_1}
                            className="menu"
                        /> */}
                        <ul className="navigation__extra">
                            <li className="nav_extra_arivanna">
                                <Link href="/vendor/become-a-vendor">
                                    <a>Sell on Arivanna</a>
                                </Link>
                            </li>
                            <li className="nav_extra_arivanna">
                                <Link href="/account/order-tracking">
                                    <a>Track your order</a>
                                </Link>
                            </li>
                            <li>
                                <CurrencyDropdown />
                            </li>
                            <li className="mini-language-switcher">
                                <LanguageSwitcher />
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            </>
        );
    }
}

export default NavigationDefault; 
