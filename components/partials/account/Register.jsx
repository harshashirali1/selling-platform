import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { register } from '../../../store/auth/action';
import { Alert } from 'reactstrap';

import { Form, Input, notification } from 'antd';
import { connect } from 'react-redux';
import api from '../../../api/handler';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user_first_name: '',
            user_middle_name: '',
            user_last_name: '',
            user_dob: '',
            user_gender: '',
            user_address_shipping: '',
            user_address_billing: '',
            user_phone_number: '',
            user_email: '',
            user_password: '',
            id_user_title: 1,
            error: '',
        };
    }

    static getDerivedStateFromProps(props) {
        if (props.isLoggedIn === true) {
            Router.push('/');
        }

        return false;
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    handleFeatureWillUpdate(e) {
        e.preventDefault();
        notification.open({
            message: 'Opp! Something went wrong.',
            description: 'This feature has been updated later!',
            duration: 500,
        });
    }

    handleSubmit = async (e) => {
        const {
            user_first_name,
            user_middle_name,
            user_last_name,
            user_dob,
            user_gender,
            user_address_shipping,
            user_address_billing,
            user_phone_number,
            user_email,
            user_password,
            id_user_title,
        } = this.state;

        const data = {
            user_first_name,
            user_middle_name,
            user_last_name,
            user_dob,
            user_gender,
            user_address_shipping,
            user_address_billing,
            user_phone_number,
            user_email,
            user_password,
            id_user_title,
        };

        const route = 'user_create';

        await api.handler
            .api_post(data, route)
            .then((response) => {
                if (!response.success) {
                    this.setState({ error: response.data });
                } else {
                    this.props.handleRegister({
                        isLoggedIn: true,
                        token: response.data.token,
                        id_user: response.data.id_user,
                    });
                }
            })
            .catch((error) => {
                throw error;
            });
    };

    render() {
        return (
            <div className="ps-my-account">
                <div className="container">
                    <Form
                        className="ps-form--account"
                        onFinish={this.handleSubmit.bind(this)}>
                        <ul className="ps-tab-list">
                            <li>
                                <Link href="/account/login">
                                    <a>Login</a>
                                </Link>
                            </li>
                            <li className="active">
                                <Link href="/account/register">
                                    <a>Register</a>
                                </Link>
                            </li>
                        </ul>

                        <div className="ps-tab active" name="register">
                            <div className="ps-form__content">
                                <h5>Register An Account</h5>
                                {this.state.error && (
                                    <Alert color="danger">
                                        {this.state.error}
                                    </Alert>
                                )}
                                <div className="form-group">
                                    <Form.Item
                                        name="user_first_name"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your first name!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_first_name"
                                            placeholder="First name"
                                            value={this.state.user_first_name}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item
                                        name="user_middle_name"
                                        rules={[
                                            {
                                                required: false,
                                                message:
                                                    'Please input your middle name!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_middle_name"
                                            placeholder="middle name"
                                            value={this.state.user_middle_name}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item
                                        name="user_last_name"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your last name!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_last_name"
                                            placeholder="last name"
                                            value={this.state.user_last_name}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item
                                        name="user_dob"
                                        rules={[
                                            {
                                                required: false,
                                                message:
                                                    'Please input your date of birth!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="date"
                                            name="user_dob"
                                            placeholder="D.O.B"
                                            value={this.state.user_dob}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item
                                        name="user_gender"
                                        rules={[
                                            {
                                                required: false,
                                                message:
                                                    'Please input your gender!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_gender"
                                            placeholder="Gender"
                                            value={this.state.user_gender}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>

                                <div>
                                    <Form.Item
                                        name="user_address_shipping"
                                        rules={[
                                            {
                                                required: false,
                                                message:
                                                    'Please input your gender!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_address_shipping"
                                            placeholder="Shipping address"
                                            value={
                                                this.state.user_address_shipping
                                            }
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item
                                        name="user_address_billing"
                                        rules={[
                                            {
                                                required: false,
                                                message:
                                                    'Please input your gender!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_address_billing"
                                            placeholder="Billing address"
                                            value={
                                                this.state.user_address_billing
                                            }
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div className="form-group">
                                    <Form.Item
                                        name="user_phone_number"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your phone number!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_phone_number"
                                            placeholder="Phone number"
                                            value={this.state.user_phone_number}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div className="form-group">
                                    <Form.Item
                                        name="user_email"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your email!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="email"
                                            name="user_email"
                                            placeholder="Email address"
                                            value={this.state.user_email}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>

                                <div className="form-group form-forgot">
                                    <Form.Item
                                        name="user_password"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your password!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="password"
                                            name="user_password"
                                            placeholder="Password..."
                                            value={this.state.user_password}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div className="form-group submit">
                                    <button
                                        type="submit"
                                        className="ps-btn ps-btn--fullwidth">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return state.auth;
};

const dispatchActionToProps = {
    handleRegister: register,
};
export default connect(mapStateToProps, dispatchActionToProps)(Register);
