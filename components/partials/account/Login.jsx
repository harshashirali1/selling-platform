import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { login } from '../../../store/auth/action';
import { Alert } from 'reactstrap';

import { Form, Input, notification } from 'antd';
import { connect } from 'react-redux';
import axios from 'axios';
import api from '../../../api/handler';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_login: '',
            user_password: '',
            error: '',
        };
    }

    static getDerivedStateFromProps(props) {
        if (props.isLoggedIn === true) {
            Router.push('/');
        }

        return false;
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    handleFeatureWillUpdate(e) {
        e.preventDefault();
        notification.open({
            message: 'Opp! Something went wrong.',
            description: 'This feature has been updated later!',
            duration: 500,
        });
    }

    // handleLoginSubmit = async(e) => {

    handleLoginSubmit = async (e) => {
        //e.preventDefault();
        // debugger;

        const { user_login, user_password } = this.state;

        let data;

        user_login.includes('@')
            ? (data = {
                  user_email: user_login,
                  user_password,
              })
            : (data = {
                  user_phone_number: user_login,
                  user_password,
              });

        const route = 'user_login';

        await api.handler
            .api_post(data, route)
            .then((response) => {
                if (!response.success) {
                    this.setState({ error: response.data });
                } else {
                    this.props.handleLogin({
                        isLoggedIn: true,
                        token: response.data.token,
                        user_access_level: response.data.user_access_level,
                        id_user: response.data.id_user,
                    });
                }
            })
            .catch((error) => {
                throw error;
            });
    };

    render() {
        return (
            // <ContainerShop title="Shop">
            <div className="ps-my-account">
                <div className="container">
                    <Form
                        className="ps-form--account"
                        onFinish={this.handleLoginSubmit.bind(this)}>
                        <ul className="ps-tab-list">
                            <li className="active">
                                <Link href="/account/login">
                                    <a>Login</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/account/register">
                                    <a>Register</a>
                                </Link>
                            </li>
                        </ul>
                        <div className="ps-tab active" id="sign-in">
                            <div className="ps-form__content">
                                <h5>Log In Your Account</h5>
                                {this.state.error && (
                                    <Alert color="danger">
                                        {this.state.error}
                                    </Alert>
                                )}
                                <div className="form-group">
                                    <Form.Item
                                        name="user_login"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your email or phone number!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="text"
                                            name="user_login"
                                            placeholder="Email address or phone number"
                                            value={this.state.user_login}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div className="form-group form-forgot">
                                    <Form.Item
                                        name="user_password"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    'Please input your password!',
                                            },
                                        ]}>
                                        <Input
                                            className="form-control"
                                            type="password"
                                            name="user_password"
                                            placeholder="Password..."
                                            value={this.state.user_password}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Item>
                                </div>
                                <div className="form-group">
                                    <div className="ps-checkbox">
                                        <input
                                            className="form-control"
                                            type="checkbox"
                                            id="remember-me"
                                            name="remember-me"
                                        />
                                        <label htmlFor="remember-me">
                                            Rememeber me
                                        </label>
                                    </div>
                                </div>
                                <div className="form-group submit">
                                    <button
                                        type="submit"
                                        className="ps-btn ps-btn--fullwidth">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
            // </ContainerShop>
        );
    }
}
const mapStateToProps = (state) => {
    return state.auth;
};

const dispatchActionToProps = {
    handleLogin: login,
};

export default connect(mapStateToProps, dispatchActionToProps)(Login);
