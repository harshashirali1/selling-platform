import React from 'react';

const ManageReturns = () => (
    <div className="ps-manage-returns">
        <div className="ps-section__header">
            <h3>Manage Returns</h3>
        </div>
        <div className="ps-section__content">
            <p>
                Print return labels and check the status of your recent returns
            </p>
        </div>
        <div className="ps-section__nav">
            <a href="#">VIEW YOUR RETURNS</a>
        </div>
    </div>
);

export default ManageReturns;
