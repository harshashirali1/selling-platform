import React from 'react';

const FAQs = () => (
    <div className="container">
        <div className="ps-section-faqs">
            <div className="ps-section__header">
                <h3>Frequently Asked Questions</h3>
            </div>
            <div className="ps-section__content">
                <div>
                    <h5>What can I return?</h5>
                    <p>
                        You may return most new, unopened items sold and
                        fulfilled by Arivanna within 30 days of delivery for a
                        full refund. During the holidays, items shipped by
                        Arivanna between November 1st and December 31st can be
                        returned until January 31st. <a href="#">Learn more</a>
                    </p>
                </div>
                <div>
                    <h5>How long before I get my refund?</h5>
                    <p>
                        The process of refund may take about 2-3 weeks from the
                        time you successfully created a return request on your
                        order. Once we have received and processed your return,
                        you will receive your full refund within 7 days.{' '}
                        <a href="#">Learn more</a>
                    </p>
                </div>
                <div>
                    <h5 className="ps-faq-question">
                        Can I replace or exchange my order?
                    </h5>
                    <p>
                        Arivanna always seeks the best satisfaction for our
                        customer so yes, you can request for replacement and
                        exchange on your items. If you have received a damaged
                        or defective item, we could ship you the exact item as
                        replacement or we could send you a different item,
                        different size or color of your choice. You just have to
                        make the request on the order by clicking return item{' '}
                        <a href="">Learn more</a>
                    </p>
                </div>
                <p>
                    For detailed information on return policy see{' '}
                    <a href="/returns-refund">Returns & Refunds</a>{' '}
                </p>
            </div>
        </div>
    </div>
);

export default FAQs;
