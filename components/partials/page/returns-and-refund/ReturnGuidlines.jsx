import React from 'react';

const ReturnGuidlines = () => (
    <div className="ps-return-guidelines">
        <div className="ps-section__header">
            <h3>Return Guidlines</h3>
        </div>
        <div className="ps-section__content">
            Shopping with Arivanna, returns are made hassle-fre and absolutely
            free. You may return most new, unopened items sold and fulfilled by
            Arivanna within 30 days of delivery for a full refund. Learn more
            about Arivanna’s Return Policy
        </div>
        <div className="ps-section__nav">
            <a href="#">VIEW ORDERS</a>
        </div>
    </div>
);

export default ReturnGuidlines;
