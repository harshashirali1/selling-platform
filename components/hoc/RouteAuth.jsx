import React from 'react';
import Login from '~/components/partials/account/Login';
import { connect } from 'react-redux';
import Error from '~/pages/page/page-404';
import LoginPage from '~/pages/account/login';
import { AccessLevel } from '~/utilities/constant-class';

const withAuth = (Component, accessLevelOfCurrentPage = 3) => {
    /* assiging default accessLevelOfCurrentPage=3*/
    const Auth = (props) => {
        console.log('from withauth');
        console.log('From store : ', props.auth);
        /* get the user information from the localstorage and compare with the  access
      levels of the current page 
      dev-1, buyer-2, public-3*/

        //  const storeObject=(JSON.parse(localStorage.getItem("persist:martfury")));
        //  const isLoggedIn=(JSON.parse(storeObject.auth).isLoggedIn);
        //  //const userName = (JSON.parse(storeObject.auth).userName);
        //  const token = (JSON.parse(storeObject.auth).token);
        //  const accessLevelOfUser=(JSON.parse(storeObject.auth).accessLevel);
        const isLoggedIn = props.auth.isLoggedIn;
        const userName = props.auth.userName;
        const token = props.auth.token;
        const accessLevelOfUser = props.auth.accessLevel;
        //always accessible to all users
        console.log('isloggedin', isLoggedIn);
        console.log('userName', userName);
        console.log('token', token);
        console.log('accessLevelOfUser', accessLevelOfUser);
        console.log('accessLevelOfCurrentPage', accessLevelOfCurrentPage);
        //before logging
        if (!isLoggedIn) {
            //|| !token) {

            if (
                accessLevelOfCurrentPage === AccessLevel.PUBLIC_ACCESS_LEVEL ||
                accessLevelOfCurrentPage === AccessLevel.ALWAYS_ACCESS_LEVEL
            ) {
                console.log(
                    'before login user can login,register and access alALWAYS_ACCESS_LEVEL',
                    accessLevelOfCurrentPage
                );
                return <Component {...props} />;
            } else {
                console.log(
                    'before login user trying to access 0,1,2,3,',
                    accessLevelOfCurrentPage
                );
                return <LoginPage />;
            }
        }
        //after loggin
        console.log(isLoggedIn);

        if (isLoggedIn) {
            // && token) {
            //return <LoginPage />;
            /* if the user tries to register or login again show no access other wise redirect to requested page*/
            console.log('Component Nname : ', Component);
            if (accessLevelOfUser.includes(accessLevelOfCurrentPage)) {
                if (
                    accessLevelOfCurrentPage === AccessLevel.PUBLIC_ACCESS_LEVEL
                ) {
                    console.log(
                        'after login user trying to access login,register  ',
                        accessLevelOfCurrentPage
                    );
                    console.log('already logged in');
                    return <Error error="401" />;
                } else {
                    // if (
                    //     accessLevelOfCurrentPage.includes(
                    //         AccessLevel.DEV_ACCESS_LEVEL
                    //     ) ||
                    //     accessLevelOfCurrentPage.includes(
                    //         AccessLevel.USER_VENDOR_TM_LEVEL
                    //     ) ||
                    //     accessLevelOfCurrentPage.includes(
                    //         AccessLevel.USER_BUYER_ACCESS_LEVEL
                    //     ) ||
                    //     accessLevelOfCurrentPage.includes(
                    //         AccessLevel.USER_ACCESS_LEVEL
                    //     )
                    // )
                    //{
                    console.log(
                        'after login user can access requested page if authorised',
                        accessLevelOfCurrentPage
                    );
                    return <Component {...props} />;
                }
            } else {
                console.log(
                    'after login user cannot access requested page if not-authorised',
                    accessLevelOfCurrentPage
                );
                return <Error error="401" />;
            }
            //return <Component {...props} />;
            // }
        }
    };

    if (Component.getInitialProps) {
        Auth.getInitialProps = Component.getInitialProps;
    }
    const mapStateToProps = (state) => {
        return {
            auth: state.auth,
        };
    };

    return connect(mapStateToProps)(Auth);
};

export default withAuth;
